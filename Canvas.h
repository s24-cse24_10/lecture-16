#ifndef CANVAS_H
#define CANVAS_H

#include "Toolbar.h"
#include "Rectangle.h"
#include "Scribble.h"

struct Canvas {
    Rectangle area;

    Shape* shapes[1000];
    int shapeCounter;
    int selectedShape;

    float offsetX;
    float offsetY;

    Canvas(){
        area = Rectangle(-0.8f, 1.0f, 1.8f, 1.8f, Color(1.0f, 1.0f, 1.0f));
        shapeCounter = 0;
        selectedShape = -1;
    }

    void handleMouseClick(float x, float y, Tool tool, Color color){
        if (tool == PENCIL){
            shapes[shapeCounter] = new Scribble();
            shapeCounter++;
            ((Scribble*)shapes[shapeCounter - 1])->addPoint(x, y, color);
        }
        else if (tool == ERASER){
            //
        }
        else if (tool == SQUARE){
            shapes[shapeCounter] = new Rectangle(x, y, 0.2f, 0.2f, color);
            shapeCounter++;
        } else if (tool == MOUSE) {
            for (int i = 0; i < shapeCounter; i++) {
                shapes[i]->deselect();
            }

            selectedShape = -1;
            for (int i = shapeCounter - 1; i >= 0; i--) {
                if (shapes[i]->contains(x, y)) {
                    shapes[i]->select();
                    selectedShape = i;
                    offsetX = x - shapes[i]->getX();
                    offsetY = shapes[i]->getY() - y;
                }
            }
        }
    }

    void handleMouseMotion(float x, float y, Tool tool, Color color) {
        if (tool == PENCIL){
            ((Scribble*)shapes[shapeCounter - 1])->addPoint(x, y, color);
        }
        else if (tool == ERASER){
            //
        }
        if (tool == MOUSE) {
            if (selectedShape != -1) {
                shapes[selectedShape]->setX(x - offsetX);
                shapes[selectedShape]->setY(y + offsetY);
            }
        }
    }

    void draw(){
        area.draw();

        for (int i = 0; i < shapeCounter; i++) {
            shapes[i]->draw();
        }
    }

    bool contains(float x, float y){
        return area.contains(x, y);
    }
};

#endif